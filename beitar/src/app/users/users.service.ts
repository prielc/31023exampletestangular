import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { HttpParams, HttpHeaders } from '@angular/common/http';
import { AngularFireDatabase } from 'angularfire2/database';

@Injectable()
export class UsersService {

  url = environment.url;

  getUsers(){
    //get users from the SLIM rest API (Don't say DB)
    return  this.http.get(this.url +'users');
  }

  getUsersFire(){
    return this.db.list('/users').valueChanges();    
  }

  postUser(data){
    let options = {
      headers: new Headers({'content-type': 'application/x-www-form-urlencoded'}
    )};
    var params = new HttpParams().append('name',data.name).append('phonenumber',data.phonenumber);
    return this.http.post(this.url +'users',params.toString(),options); 
  }

  deleteUser(id){
    //let options = {
    //  headers: new Headers({'content-type': 'application/x-www-form-urlencoded'}
    //)}; 
    console.log(this.url +'users/'+id);   
    return this.http.delete(this.url +'users/'+id); 
  }

  constructor(private http:Http, private db:AngularFireDatabase) { 
    
  }
  
}